import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick, handleAdd) {
    super();

    this.createFighter(fighter, handleClick, handleAdd);
  }

  createFighter(fighter, handleClick, handleAdd) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const addButton = this.createAddButton()
    
    addButton.addEventListener('click', event => handleAdd(event, fighter))
    
    nameElement.append(addButton)

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  createAddButton() {
    const addButton = this.createElement({ 
      tagName: 'button', 
      className: 'btn btn-success btn-add ml-2', 
      attributes : { 
        type: 'button',
        'data-toggle': 'tooltip', 
        'data-placement': 'top',
        title: 'Add fighter to arena'
      } 
    })
    const plusIcon = this.createElement({ tagName: 'i', className: 'fas fa-plus' })
    const minusIcon = this.createElement({ tagName: 'i', className: 'fas fa-minus d-none' })
    addButton.append(plusIcon, minusIcon)

    return addButton
  }
}

export default FighterView;