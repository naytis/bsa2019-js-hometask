import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import FightArenaView from './fightArenaView';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;
      const fightArenaView = new FightArenaView(fightersView.fightArenaMap)
      const fightButton = fightArenaView.fightButton

      App.rootElement.append(fightersElement, fightButton);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;