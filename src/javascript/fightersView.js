import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from './fighter';
import fight from './fight';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.handleAdd = this.handleAddButtonClick.bind(this)
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  fightersMap = new Map();
  fightArenaMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick, this.handleAdd);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    if (event.target.closest('.btn-add') != null) { 
      return
    }

    const details = await this.getFighterDetails(fighter)
    const form = this.createForm(details)

    const submitButton = document.querySelector('#save-details')
    submitButton.addEventListener('click', () => {
      let form = new FormData(document.querySelector('#edit-details'))
      let updatedFighter = Object.fromEntries(form.entries())
      this.fightersDetailsMap.set(fighter._id, updatedFighter)
      $('#fighter-modal').modal('hide')
    })

    $('#fighter-modal').on(
      'show.bs.modal', 
      e => $('#modal-body').html(form)
    )
    $('#fighter-modal').modal('show')
  }

  async handleAddButtonClick(event, fighterElement) {
    let button = event.target.closest('.btn-add')
    let icons = button.querySelectorAll('svg')
    for (let icon of icons) {
      icon.classList.toggle('d-none')
    }
    button.classList.toggle('btn-success')
    button.classList.toggle('btn-secondary')
    
    if (!this.fightersMap.has(fighterElement._id)) {
      let fighterDetails = await this.getFighterDetails(fighterElement)
      this.fightersMap.set(fighterElement._id, new Fighter(fighterDetails))
    }
    let fighter = this.fightersMap.get(fighterElement._id)

    if (button.classList.contains('added')) {

      if (this.fightArenaMap.size == 2) {
        let buttons = document.querySelectorAll('button.btn-add')
        for (let button of buttons) {
          button.removeAttribute('disabled')
        }
        document.querySelector('.btn-fight').toggleAttribute('disabled')
      }

      this.fightArenaMap.delete(fighterElement._id, fighter)
      button.classList.toggle('added')
      button.closest('.fighter').classList.toggle('added')

    } else {

      this.fightArenaMap.set(fighterElement._id, fighter)
      button.classList.toggle('added')
      button.closest('.fighter').classList.toggle('added')

      if (this.fightArenaMap.size == 2) {
        let buttons = document.querySelectorAll('button.btn-add:not(.added)')
        for (let button of buttons) {
          button.toggleAttribute('disabled')
        }

        document.querySelector('.btn-fight').toggleAttribute('disabled')
      }

    }
    
    console.log(this.fightArenaMap)

  }

  async getFighterDetails(fighter) {
    if (!this.fightersDetailsMap.has(fighter._id)) {
      const fighterDetails = await fighterService.getFighterDetails(fighter._id)
      this.fightersDetailsMap.set(fighter._id, fighterDetails);
    }

    return this.fightersDetailsMap.get(fighter._id)
  }

  createForm(details) {
    const form = this.createElement({ 
      tagName: 'form', 
      className: 'classnamehere',
      attributes: { id: 'edit-details' } 
    })

    Object.keys(details).forEach(key => {
      const formGroup = this.createElement({ 
        tagName: 'div', 
        className: 'form-group row' 
      })
      
      const label = this.createElement({ 
        tagName: 'label',
        className: 'col-3 col-form-label text-center',
        attributes: { 'for': `edit-detail-${key}` } 
      })
      const labelText = key.startsWith('_') ? key.slice(1) : key
      label.innerText = labelText.charAt(0).toUpperCase() + labelText.slice(1)

      let input;

      if (key == 'health' || key == 'attack') {
        input = this.createElement({ 
          tagName: 'input', 
          className: 'form-control col-8', 
          attributes: { 
            id: `edit-detail-${key}`,
            type: 'number',
            name: key,
            value: details[key],
          } 
        })
      } else {
        input = this.createElement({ 
          tagName: 'input', 
          className: 'form-control-plaintext col-8', 
          attributes: { 
            id: `edit-detail-${key}`,
            type: 'text',
            name: key,
            value: details[key],
            readonly: true
          } 
        })
      }

      formGroup.append(label, input)
      form.append(formGroup)
    });

    return form
  }
}

export default FightersView;